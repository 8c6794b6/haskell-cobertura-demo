# haskell-cobertura-demo

A sample repository to show [test coverage
visualization](https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html)
for Haskell source codes with [hpc-codecov][hpc-codecov].

[![screenshot](/img/screenshot.png "sample merge request")][mr2diff]


## Sample merge request and CI settings

This repository has a sample [merge request][mr2diff] to show the test
coverage of Haskell source codes in the diff. The following describes
sample CI settings to generate a Cobertura XML report to visualize
test coverage in merge requests.

### Using the ``hpc-codecov`` container image

The [GitLab CI
configuration](https://gitlab.com/8c6794b6/haskell-cobertura-demo/-/raw/main/.gitlab-ci.yml?ref_type=heads)
of this repository builds and tests a sample Cabal package with the
code coverage option enabled:

```yaml
build-and-test-my-project:
  image: haskell:slim
  extends:
    - .on-merge-request
    - .cache-dist-newstyle
  stage: build
  script:
    - ghc --version
    - cabal --version
    - cabal build
    - cabal test
    - cabal haddock
```

After running the tests, the CI generates a Cobertura XML report with
a container image containing the [hpc-codecov][hpc-codecov]
executable and uploads the coverage report artifact:

```yaml
coverage:
  image: ghcr.io/8c6794b6/hpc-codecov
  extends:
    - .on-merge-request
    - .cache-dist-newstyle
  stage: test
  script:
    - hpc-codecov --version
    - hpc-codecov --verbose -xMain --format=cobertura -o coverage.xml cabal:my-project-test
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
  
```

Note that the above two jobs extend ``.on-merge-request`` and
``.cache-dist-newstyle``, which are defined as:

```yaml
.on-merge-request:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

.cache-dist-newstyle:
  cache:
    key: v0-my-project-${CI_COMMIT_REF_SLUG}
    when: always
    paths:
      - "dist-newstyle/**/*"
```

### Manually obtaining the ``hpc-codecov`` executable

It is not necessary to use the dedicated container image to run the
``hpc-codecov`` executable. One can fetch the ``hpc-codecov``
executable (via [curl](https://curl.se/), 
[wget](https://www.gnu.org/software/wget/), or your favorite
downloader) from the [latest releases
page](https://github.com/8c6794b6/hpc-codecov/releases/latest), or
install from
[Hackage](https://hackage.haskell.org/package/hpc-codecov) via
``cabal-install``. Then, the above `coverage` job could be removed,
and the `script` section of the `build-and-test-my-project` job could
be modified as:

```yaml
build-and-test-my-project:
  image: haskell:slim
  extends:
    - .on-merge-request
  stage: build
  script:
    - ghc --version
    - cabal --version
    - cabal build
    - cabal test
    - cabal haddock
    - curl -sL -o hpc-codecov https://github.com/8c6794b6/hpc-codecov/releases/download/v0.5.0.0/hpc-codecov-Linux
    - chmod +x hpc-codecov
    - ./hpc-codecov --verbose -xMain --format=cobertura -o coverrage.xml cabal:my-project-test
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
```


<!-- urls -->

[mr2diff]: https://gitlab.com/8c6794b6/haskell-cobertura-demo/-/merge_requests/2/diffs
[hpc-codecov]: https://github.com/8c6794b6/hpc-codecov


